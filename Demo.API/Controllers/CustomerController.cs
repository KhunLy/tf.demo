﻿using Demo.API.DTO;
using Demo.API.DTO.Customer;
using Demo.API.Mappers;
using Demo.BLL.Exceptions;
using Demo.BLL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly CustomerService _customerService;

        public CustomerController(CustomerService customerService, CountryService countryService)
        {
            _customerService = customerService;
        }

        [HttpGet]
        [Produces(typeof(IndexDTO<CustomerSimpleDTO>))]
        public IActionResult Get([FromQuery]CustomerFilterDTO filter)
        {
            IEnumerable<CustomerSimpleDTO> dto
                = _customerService.Get(filter.Limit, filter.Offset, filter.Search, filter.CountryIso)
                    .Select(c => c.ToSimpleDTO());
            int total = _customerService.Count(filter.Search, filter.CountryIso);
            return Ok(new IndexDTO<CustomerSimpleDTO>(total, dto));
        }

        [HttpGet("{id}")]
        [Produces(typeof(CustomerDetailDTO))]
        public IActionResult Get(int id)
        {
            try
            {
                return Ok(_customerService.Get(id).ToDetailDTO());
            }
            catch(KeyNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post(CustomerFormDTO form)
        {
            try
            {
                await _customerService.Add(form.ToModel());
                return NoContent();
            }
            catch(UniqueConstraintException ex)
            {
                foreach(string f in ex.Fields)
                {
                    ModelState.AddModelError(f, ex.Message);
                }
                return BadRequest(ModelState);
            }
            catch(ReferenceException ex)
            {
                ModelState.AddModelError(ex.FieldName, ex.Message);
                return BadRequest(ModelState);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _customerService.Delete(id);
                return NoContent();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
        }
    }
}
