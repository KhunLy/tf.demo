﻿using Demo.API.DTO;
using Demo.API.DTO.Invoice;
using Demo.API.Mappers;
using Demo.BLL.Exceptions;
using Demo.BLL.Models;
using Demo.BLL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InvoiceController : ControllerBase
    {
        private readonly InvoiceService _invoiceService;

        public InvoiceController(InvoiceService invoiceService)
        {
            _invoiceService = invoiceService;
        }

        [HttpGet]
        [Produces(typeof(IndexDTO<InvoiceSimpleDTO>))]
        public IActionResult Get([FromQuery]InvoiceFilterDTO filter)
        {
            IEnumerable<InvoiceSimpleDTO> results = _invoiceService
                .Get(filter.Limit, filter.Offset, filter.CustomerId)
                .Select(i => i.ToSimpleDTO());
            int count = _invoiceService.Count(filter.CustomerId);

            return Ok(new IndexDTO<InvoiceSimpleDTO>(count, results));
        }

        [HttpGet("{reference}")]
        [Produces(typeof(InvoiceDetailDTO))]
        public IActionResult Get(string reference)
        {
            try
            {
                return Ok(_invoiceService.Get(reference)
                    .ToDetailDTO());
            }
            catch(KeyNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPost]
        public IActionResult Post(InvoiceFormDTO form)
        {
            try
            {
                _invoiceService.Add(form.ToModel());
                return NoContent();
            }
            catch(ReferenceException ex)
            {
                ModelState.AddModelError(ex.FieldName, ex.Message);
                return BadRequest(ModelState);
            }
        }

        [HttpPatch("{reference}")]
        public IActionResult Patch(string reference, InvoiceFormDTO form)
        {
            try
            {
                _invoiceService.Update(reference, form.ToModel());
                return NoContent();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpDelete("{reference}")]
        public IActionResult Delete(string reference)
        {
            try
            {
                _invoiceService.Delete(reference);
                return NoContent();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
        }
    }
}
