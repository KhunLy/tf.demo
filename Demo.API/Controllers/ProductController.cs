﻿using Demo.API.DTO.Product;
using Demo.API.Mappers;
using Demo.BLL.Exceptions;
using Demo.BLL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly ProductService _productService;

        public ProductController(ProductService productService)
        {
            _productService = productService;
        }

        [HttpPost]
        public IActionResult Post(ProductFormDTO form)
        {
            try
            {
                _productService.Add(form.ToModel());
                return NoContent();
            }
            catch(UniqueConstraintException ex) 
            {
                foreach (string f in ex.Fields)
                {
                    ModelState.AddModelError(f, ex.Message);
                }
                return BadRequest(ModelState);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, ProductFormDTO form)
        {
            try
            {
                _productService.Update(id, form.ToModel());
                return NoContent();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
            catch(FieldChangedException ex)
            {
                ModelState.AddModelError(ex.FieldName, ex.Message);
                return BadRequest(ModelState);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _productService.Delete(id);
                return NoContent();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
        }
    }
}
