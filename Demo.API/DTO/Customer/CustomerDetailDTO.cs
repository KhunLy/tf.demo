﻿using Demo.API.DTO.Invoice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.API.DTO.Customer
{
    public class CustomerDetailDTO
    {
        public int Id { get; set; }
        public string Reference { get; set; }
        public string Email { get; set; }
        public string CountryIso { get; set; }
        public IEnumerable<InvoiceSimpleDTO> Invoices { get; set; }
    }
}
