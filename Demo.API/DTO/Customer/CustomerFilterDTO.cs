﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.API.DTO.Customer
{
    public class CustomerFilterDTO
    {
        public int Limit { get; set; } = 10;
        public int Offset { get; set; } = 0;
        public string Search { get; set; }
        public string CountryIso { get; set; }
    }
}
