﻿using System.ComponentModel.DataAnnotations;

namespace Demo.API.DTO.Customer
{
    public class CustomerFormDTO
    {
        [Required]
        [MaxLength(255)]
        public string Email { get; set; }
        [Required]
        [StringLength(2, MinimumLength = 2)]
        public string CountryIso { get; set; }
    }
}
