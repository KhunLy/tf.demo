﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.API.DTO
{
    public class IndexDTO<T>
        where T: class
    {
        public int Total { get; private set; }
        public IEnumerable<T> Results { get; private set; }
        public IndexDTO(int total, IEnumerable<T> results)
        {
            Total = total;
            Results = results;
        }
    }
}
