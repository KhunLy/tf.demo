﻿using Demo.API.DTO.InvoiceLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.API.DTO.Invoice
{
    public class InvoiceDetailDTO
    {
        public string Reference { get; set; }
        public DateTime Date { get; set; }
        public int CustomerId { get; set; }
        public IEnumerable<InvoiceLineDetailDTO> InvoiceLines { get; set; }
    }
}
