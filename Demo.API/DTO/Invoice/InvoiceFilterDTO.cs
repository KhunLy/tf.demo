﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.API.DTO.Invoice
{
    public class InvoiceFilterDTO
    {
        public int Limit { get; set; } = 10;
        public int Offset { get; set; } = 0;
        public int? CustomerId { get; set; }
    }
}
