﻿using Demo.API.DTO.InvoiceLine;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.API.DTO.Invoice
{
    public class InvoiceFormDTO
    {
        [Required]
        public int CustomerId { get; set; }
        public IEnumerable<InvoiceLineFormDTO> InvoiceLines { get; set; }
    }
}
