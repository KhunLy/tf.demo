﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.API.DTO.Invoice
{
    public class InvoiceSimpleDTO
    {
        public string Reference { get; set; }
        public DateTime Date { get; set; }
        public int CustomerId { get; set; }
    }
}
