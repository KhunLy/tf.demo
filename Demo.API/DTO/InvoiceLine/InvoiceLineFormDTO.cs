﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.API.DTO.InvoiceLine
{
    public class InvoiceLineFormDTO
    {
        [Required]
        public int ProductId { get; set; }

        [Required]
        [Range(1,9999)]
        public int Quantity { get; set; }
    }
}
