﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.API.DTO.Product
{
    public class ProductFormDTO
    {
        [Required]
        [RegularExpression("[a-zA-Z0-9 _]*")]
        [StringLength(50, MinimumLength = 5)]
        public string Reference { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        [Range(0, 999999.99)]
        public decimal Price { get; set; }
    }
}
