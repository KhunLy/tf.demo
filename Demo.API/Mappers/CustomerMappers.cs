﻿using Demo.API.DTO.Customer;
using Demo.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.API.Mappers
{
    static class CustomerMappers
    {
        public static CustomerModel ToModel(this CustomerFormDTO dto)
        {
            return new CustomerModel
            {
                Email = dto.Email,
                CountryIso = dto.CountryIso,
            };
        }

        public static CustomerSimpleDTO ToSimpleDTO(this CustomerModel model)
        {
            return new CustomerSimpleDTO
            {
                Email = model.Email,
                Id = model.Id,
                CountryIso = model.CountryIso,
                Reference = model.Reference
            };
        }

        public static CustomerDetailDTO ToDetailDTO(this CustomerModel model)
        {
            return new CustomerDetailDTO
            {
                Email = model.Email,
                Id = model.Id,
                CountryIso = model.CountryIso,
                Reference = model.Reference,
                Invoices = model.Invoices.Select(i => i.ToSimpleDTO())
            };
        }
    }
}
