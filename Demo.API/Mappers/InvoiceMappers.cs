﻿using Demo.API.DTO.Invoice;
using Demo.API.DTO.InvoiceLine;
using Demo.BLL.Models;
using System.Linq;

namespace Demo.API.Mappers
{
    static class InvoiceMappers
    {
        public static InvoiceModel ToModel(this InvoiceFormDTO dto)
        {
            return new InvoiceModel
            {
                CustomerId = dto.CustomerId,
                InvoiceLines = dto.InvoiceLines.Select(il => new InvoiceLineModel { 
                    ProductId = il.ProductId,
                    Quantity = il.Quantity,
                })
            };
        }

        public static InvoiceSimpleDTO ToSimpleDTO(this InvoiceModel model)
        {
            return new InvoiceSimpleDTO
            {
                Reference = model.Reference,
                CustomerId = model.CustomerId,
                Date = model.Date
            };
        }

        public static InvoiceDetailDTO ToDetailDTO(this InvoiceModel model)
        {
            return new InvoiceDetailDTO
            {
                Reference = model.Reference,
                CustomerId = model.CustomerId,
                Date = model.Date,
                InvoiceLines = model.InvoiceLines.Select(il => new InvoiceLineDetailDTO { 
                    ProductId = il.ProductId,
                    ProductName = il.ProductName,
                    ProductPrice = il.ProductPrice,
                    ProductReference = il.ProductReference,
                    Quantity = il.Quantity
                })
            };
        }
    }
}
