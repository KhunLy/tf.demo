﻿using Demo.API.DTO.Product;
using Demo.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.API.Mappers
{
    static class ProductMappers
    {
        public static ProductModel ToModel(this ProductFormDTO dto)
        {
            return new ProductModel
            {
                Reference = dto.Reference,
                Name = dto.Name,
                Description = dto.Description,
                Price = dto.Price
            };
        }
    }
}
