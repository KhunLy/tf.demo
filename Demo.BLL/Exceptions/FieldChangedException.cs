﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.BLL.Exceptions
{
    public class FieldChangedException: Exception
    {
        public string FieldName { get; private set; }
        public FieldChangedException(string fieldName)
            : base($"The field {fieldName} cannot be updated")
        {
            FieldName = fieldName;
        }
    }
}
