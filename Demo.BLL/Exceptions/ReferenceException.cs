﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.BLL.Exceptions
{
    public class ReferenceException : Exception
    {
        public string FieldName { get; private set; }
        public object Value { get; private set; }

        public ReferenceException(string fieldName, object value)
            : base($"The field {value} does not exist")
        {
            FieldName = fieldName;
            Value = value;
        }
    }
}
