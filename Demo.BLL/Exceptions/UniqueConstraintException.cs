﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.BLL.Exceptions
{
    public class UniqueConstraintException : Exception
    {
        public string[] Fields { get; private set; }

        public UniqueConstraintException(params string[] fields)
            : base(fields.Length != 1 ? $"The fields {string.Join(", ", fields)} already exists" : $"The fields {fields.First()} already exist")
        {
            Fields = fields;
        }
    }
}
