﻿using Demo.BLL.Models;
using Demo.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.BLL.Mappers
{
    static class CustomerMappers
    {
        public static Customer ToEntity(this CustomerModel model, Action<Customer> action = null)
        {
            Customer result = new Customer
            {
                Id = model.Id,
                CountryIso = model.CountryIso,
                Email = model.Email
            };
            action?.Invoke(result);
            return result;
        }

        public static CustomerModel ToSimpleModel(this Customer entity)
        {
            return new CustomerModel
            {
                Id = entity.Id,
                Email = entity.Email,
                CountryIso = entity.CountryIso
            };
        }

        public static CustomerModel ToDetailModel(this Customer entity)
        {
            return new CustomerModel
            {
                Id = entity.Id,
                Email = entity.Email,
                CountryIso = entity.CountryIso,
                Invoices = entity.Invoices.Select(i => i.ToSimpleModel())
            };
        }
    }
}
