﻿using Demo.BLL.Models;
using Demo.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.BLL.Mappers
{
    static class InvoiceLineMappers
    {
        public static InvoiceLineModel ToSimpleModel(this InvoiceLine entity)
        {
            return new InvoiceLineModel
            {
                ProductId = entity.ProductId,
                Quantity = entity.Quantity,
                ProductReference = entity.Product.Reference,
                ProductName = entity.Product.Name,
                ProductPrice = entity.Product.Price,
            };
        }
    }
}
