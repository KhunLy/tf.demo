﻿using Demo.BLL.Models;
using Demo.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.BLL.Mappers
{
    static class InvoiceMappers
    {
        public static InvoiceModel ToSimpleModel(this Invoice entity)
        {
            return new InvoiceModel
            {
                Reference = entity.Reference,
                Date = entity.Date,
                CustomerId = entity.CustomerId
            };
        }

        public static InvoiceModel ToDetailModel(this Invoice entity)
        {
            return new InvoiceModel
            {
                Reference = entity.Reference,
                Date = entity.Date,
                CustomerId = entity.CustomerId,
                InvoiceLines = entity.InvoiceLines.Select(il => il.ToSimpleModel())
            };
        }
    }
}
