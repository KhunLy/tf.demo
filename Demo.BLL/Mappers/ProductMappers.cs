﻿using Demo.BLL.Models;
using Demo.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.BLL.Mappers
{
    static class ProductMappers
    {
        public static Product ToEntity(this ProductModel model, Action<Product> action = null)
        {
            Product p = new Product
            {
                Id = model.Id,
                Reference = model.Reference,
                Description = model.Description,
                Name = model.Name,
                Price = model.Price,
            };
            action?.Invoke(p);
            return p;
        }
    }
}
