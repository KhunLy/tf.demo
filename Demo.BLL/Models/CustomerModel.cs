﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.BLL.Models
{
    public class CustomerModel
    {
        public int Id { get; set; }
        public string Reference { get { return CreateReference(Id); } }
        public string Email { get; set; }
        public string CountryIso { get; set; }
        public IEnumerable<InvoiceModel> Invoices { get; set; }

        public static string CreateReference(int id)
        {
            return $"CUST{id:D6}";
        }
    }
}
