﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.BLL.Models
{
    public class InvoiceLineModel
    {
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public string ProductReference { get; set; }
        public string ProductName { get; set; }
        public decimal ProductPrice { get; set; }
    }
}
