﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.BLL.Models
{
    public class InvoiceModel
    {
        public string Reference { get; set; }
        public DateTime Date { get; set; }
        public int CustomerId { get; set; }
        public CustomerModel Customer { get; set; }
        public IEnumerable<InvoiceLineModel> InvoiceLines { get; set; }
    }
}
