﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Demo.BLL.Services
{
    public class CountryService
    {
        private readonly HttpClient _client;

        public CountryService(HttpClient client)
        {
            _client = client;
        }

        public async Task<bool> Exists(string code)
        {
            HttpResponseMessage message = await _client.GetAsync($"https://restcountries.com/v3.1/alpha/{code}");
            if(message.IsSuccessStatusCode)
            {
                string json = await message.Content.ReadAsStringAsync();
                IEnumerable<object> items = JsonConvert.DeserializeObject<IEnumerable<object>>(json);
                return items.Count() > 0;
            }
            throw new HttpRequestException();
        }
    }
}
