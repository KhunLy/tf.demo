﻿using Demo.BLL.Exceptions;
using Demo.BLL.Mappers;
using Demo.BLL.Models;
using Demo.DAL.Entities;
using Demo.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.BLL.Services
{
    public class CustomerService
    {
        private readonly CustomerRepository _customerRepository;
        private readonly CountryService _countryService;

        public CustomerService(CustomerRepository customerRepository, CountryService countryService)
        {
            _customerRepository = customerRepository;
            _countryService = countryService;
        }

        public async Task Add(CustomerModel model)
        {
            if(_customerRepository.Exists(c => c.Email == model.Email))
            {
                throw new UniqueConstraintException(nameof(model.Email));
            }
            if (!await _countryService.Exists(model.CountryIso))
            {
                throw new ReferenceException(nameof(model.CountryIso), model.CountryIso);
            }
            Customer toAdd =  model.ToEntity(c => c.IsActive = true);
            _customerRepository.Add(toAdd);
        }

        public IEnumerable<CustomerModel> Get(int limit, int offset, string search, string countryIso)
        {

            return _customerRepository.FindWithFilters(limit, offset, search, countryIso)
                .Select(c => c.ToSimpleModel());
        }

        public int Count(string search, string countryIso)
        {
            return _customerRepository.CountWithFilters(search, countryIso);
        }

        public CustomerModel Get(int id)
        {
            CustomerModel model = _customerRepository
                .FindOneWithInvoicesById(id)
                ?.ToDetailModel();
            if (model is null) throw new KeyNotFoundException();
            return model;
        }

        public void Delete(int id)
        {
            Customer customer = _customerRepository.Find(id);
            if (customer is null)
            {
                throw new KeyNotFoundException();
            }
            customer.IsActive = false;
            _customerRepository.Update(customer);
        }
    }
}
