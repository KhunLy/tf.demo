﻿using Demo.BLL.Exceptions;
using Demo.BLL.Mappers;
using Demo.BLL.Models;
using Demo.DAL.Entities;
using Demo.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.BLL.Services
{
    public class InvoiceService
    {
        private readonly InvoiceRepository _invoiceRepository;
        private readonly CustomerRepository _customerRepository;

        public InvoiceService(InvoiceRepository invoiceRepository, CustomerRepository customerRepository)
        {
            _invoiceRepository = invoiceRepository;
            _customerRepository = customerRepository;
        }

        public IEnumerable<InvoiceModel> Get(int limit, int offset, int? customerId)
        {
            return _invoiceRepository.FindWithFilters(limit, offset, customerId)
                .Select(i => i.ToSimpleModel());
        }

        public int Count(int? customerId)
        {
            return _invoiceRepository.CountWithFilters(customerId);
        }

        public InvoiceModel Get(string reference)
        {
            InvoiceModel model = _invoiceRepository.FindOneWithCustomerLinesAndProductsByReference(reference)
                ?.ToDetailModel();
            if(model is null)
            {
                throw new KeyNotFoundException();
            }
            return model;
        }

        public void Add(InvoiceModel model)
        {
            if(!_customerRepository.Exists(c => c.Id == model.CustomerId))
            {
                throw new ReferenceException(nameof(model.CustomerId), model.CustomerId);
            }

            DateTime today = DateTime.Now;
            Invoice lastInvoice = _invoiceRepository.FindLastByDate(today);

            string refNumberString = lastInvoice?.Reference.Substring(lastInvoice.Reference.Length - 4);
            int.TryParse(refNumberString, out int refNumber);

            Invoice invoice = _invoiceRepository.Add(new Invoice
            {
                CustomerId = model.CustomerId,
                Date = today,
                Reference = $"{today.ToString("yyMMdd")}{++refNumber:D4}",
                InvoiceLines = model.InvoiceLines.Select(p => new InvoiceLine
                {
                    ProductId = p.ProductId,
                    Quantity = p.Quantity
                }).ToList()
            });
        }

        public void Delete(string reference)
        {
            Invoice invoice = _invoiceRepository.Find(reference);
            if (invoice is null)
            {
                throw new KeyNotFoundException();
            }
            _invoiceRepository.Delete(invoice);
        }

        public void Update(string reference, InvoiceModel model)
        {
            Invoice invoice = _invoiceRepository.FindOneWithLinesByReference(reference);
            if (invoice is null)
            {
                throw new KeyNotFoundException();
            }

            invoice.InvoiceLines = model.InvoiceLines.Select(p => new InvoiceLine
            {
                ProductId = p.ProductId,
                Quantity = p.Quantity
            }).ToList();

            _invoiceRepository.Update(invoice);
        }
    }
}