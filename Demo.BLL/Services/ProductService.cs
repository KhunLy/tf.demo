﻿using Demo.BLL.Exceptions;
using Demo.BLL.Mappers;
using Demo.BLL.Models;
using Demo.DAL.Entities;
using Demo.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Demo.BLL.Services
{
    public class ProductService
    {
        private readonly ProductRepository _productRepository;

        public ProductService(ProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public void Add(ProductModel model)
        {
            if(_productRepository.Exists(p => p.Reference == model.Reference ))
            {
                throw new UniqueConstraintException(nameof(model.Reference));
            }
            _productRepository.Add(model.ToEntity(p => p.StartDate = DateTime.Now));
        }

        public void Update(int id, ProductModel model)
        {
            Product old = _productRepository.Find(id);
            if(old is null || old.EndDate != null)
            {
                throw new KeyNotFoundException();
            }
            if(model.Reference != old.Reference)
            {
                throw new FieldChangedException(nameof(model.Reference));
            }

            using (TransactionScope transaction = new TransactionScope())
            {
                DateTime now = DateTime.Now;
                old.EndDate = now;
                _productRepository.Update(old);
                _productRepository.Add(model.ToEntity(p => p.StartDate = now));
                transaction.Complete();
            }
        }

        public void Delete(int id)
        {
            Product old = _productRepository.Find(id);
            if (old is null || old.EndDate != null)
            {
                throw new KeyNotFoundException();
            }
            old.EndDate = DateTime.Now;
            _productRepository.Update(old);
        }
    }
}
