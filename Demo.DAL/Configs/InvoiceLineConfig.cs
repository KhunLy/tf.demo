﻿using Demo.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.DAL.Configs
{
    class InvoiceLineConfig : IEntityTypeConfiguration<InvoiceLine>
    {
        public void Configure(EntityTypeBuilder<InvoiceLine> builder)
        {
            builder.HasKey(il => new { il.InvoiceReference, il.ProductId });

            builder.Property(il => il.Quantity).IsRequired();

            builder.HasOne(il => il.Product).WithMany(p => p.InvoiceLines).OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(il => il.Invoice).WithMany(i => i.InvoiceLines).OnDelete(DeleteBehavior.Cascade);
        }
    }
}
