﻿using Demo.DAL.Configs;
using Demo.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace Demo.DAL
{
    public class DemoContext: DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<InvoiceLine> InvoiceLines { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DemoContext(DbContextOptions options)
            : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new CustomerConfig());
            builder.ApplyConfiguration(new ProductConfig());
            builder.ApplyConfiguration(new InvoiceLineConfig());
            builder.ApplyConfiguration(new InvoiceConfig());
        }
    }
}
