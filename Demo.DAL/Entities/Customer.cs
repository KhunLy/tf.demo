﻿using System.Collections.Generic;

namespace Demo.DAL.Entities
{
    public class Customer
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string CountryIso { get; set; }
        public bool IsActive { get; set; }
        public ICollection<Invoice> Invoices { get; set; }
    }
}
