﻿using System;
using System.Collections.Generic;

namespace Demo.DAL.Entities
{
    public class Invoice
    {
        // primary key unique business ref reference 10 char yymmddxxxx (0001 first Invoice of the day)
        public string Reference { get; set; }
        public DateTime Date { get; set; }
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
        public ICollection<InvoiceLine> InvoiceLines { get; set; }
    }
}
