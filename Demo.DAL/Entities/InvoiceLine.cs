﻿namespace Demo.DAL.Entities
{
    public class InvoiceLine
    {
        public int ProductId { get; set; }
        public string InvoiceReference { get; set; }
        public int Quantity { get; set; }
        public Product Product { get; set; }
        public Invoice Invoice { get; set; }
    }
}
