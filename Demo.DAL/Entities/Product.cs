﻿using System;
using System.Collections.Generic;

namespace Demo.DAL.Entities
{
    public class Product
    {
        public int Id { get; set; }
        public string Reference { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        // auto generated date set on insert and update
        public DateTime StartDate { get; set; }
        // auto generated date set on update
        public DateTime? EndDate { get; set; }
        public ICollection<InvoiceLine> InvoiceLines { get; set; }
    }
}
