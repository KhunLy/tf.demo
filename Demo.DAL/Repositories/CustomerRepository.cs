﻿using Demo.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.DAL.Repositories
{
    public class CustomerRepository : RepositoryBase<Customer>
    {
        public CustomerRepository(DemoContext context) : base(context)
        {
        }

        public Customer FindOneWithInvoicesById(int id)
        {
            return _context.Customers
                .Include(c => c.Invoices)
                .FirstOrDefault(c => c.Id == id);
        }

        public IEnumerable<Customer> FindWithFilters(int limit, int offset, string search, string countryIso)
        {
            return _context.Customers
                .Where(c => c.IsActive)
                .Where(c => search == null || c.Email.StartsWith(search))
                .Where(c => countryIso == null || c.CountryIso == countryIso)
                .Skip(offset)
                .Take(limit);
        }

        public int CountWithFilters(string search, string countryIso)
        {
            return _context.Customers
                .Where(c => c.IsActive)
                .Where(c => search == null || c.Email.StartsWith(search))
                .Where(c => countryIso == null || c.CountryIso == countryIso)
                .Count();
        }
    }
}
