﻿using Demo.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Demo.DAL.Repositories
{
    public class InvoiceRepository : RepositoryBase<Invoice>
    {
        public InvoiceRepository(DemoContext context) : base(context)
        {
        }

        public Invoice FindOneWithCustomerLinesAndProductsByReference(string reference)
        {
            return _context.Invoices
                .Include(i => i.Customer)
                .Include(i => i.InvoiceLines)
                .ThenInclude(il => il.Product)
                .FirstOrDefault(i => i.Reference == reference);
        }

        public Invoice FindOneWithLinesByReference(string reference)
        {
            return _context.Invoices
                .Include(i => i.InvoiceLines)
                .FirstOrDefault(i => i.Reference == reference);
        }

        public Invoice FindLastByDate(DateTime date)
        {
            return _context.Invoices
                .Where(i => i.Date.Date == date.Date)
                .OrderByDescending(i => i.Date)
                .FirstOrDefault();
        }
        public IEnumerable<Invoice> FindWithFilters(int limit, int offset, int? customerId)
        {
            return _context.Invoices.Where(i => customerId == null || i.CustomerId == customerId)
                .Skip(offset)
                .Take(limit);
        }

        public int CountWithFilters(int? customerId)
        {
            return _context.Invoices.Where(i => customerId == null || i.CustomerId == customerId)
                .Count();
        }
    }
}
