﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Linq;

namespace Demo.DAL.Repositories
{
    public abstract class RepositoryBase<T>
        where T: class
    {
        protected readonly DemoContext _context;

        protected RepositoryBase(DemoContext context)
        {
            _context = context;
        }

        public virtual bool Exists(Func<T, bool> predicate)
        {
            return _context.Set<T>().Any(predicate);
        }

        public virtual IQueryable<T> Get()
        {
            return _context.Set<T>();
        }

        public virtual T Find(params object[] keys)
        {
            return _context.Set<T>().Find(keys);
        }

        public virtual T Add(T entity)
        {
            EntityEntry entry = _context.Add(entity);
            entry.State = EntityState.Added;
            _context.SaveChanges();
            return (T)entry.Entity;
        }

        public virtual T Update(T entity)
        {
            EntityEntry entry = _context.Update(entity);
            entry.State = EntityState.Modified;
            _context.SaveChanges();
            return (T)entry.Entity;
        }

        public virtual T Delete(T entity)
        {
            EntityEntry entry = _context.Remove(entity);
            entry.State = EntityState.Deleted;
            _context.SaveChanges();
            return (T)entry.Entity;
        }
    }
}
